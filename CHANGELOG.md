## [v2.2.0](https://github.com/bsara/eslint-config-bsara/tree/v2.2.0) - 2017-08-08

#### Updated Rules

- [`brace-style`][rule-brace-style]: Added option `{ allowSingleLine: true }`
- [`indent`][rule-indent]: Add/updated options...
  - `flatTernaryExpressions: false`
  - `FunctionDeclaration: { body: 1, parameters: 'first' }`
  - `FunctionExpression: { body: 1, parameters: 'first' }`
  - `ImportDeclaration: 'first'`
  - `outerIIFEBody: 1`
  - `VariableDeclarator: { var: 2, let: 2, const: 3 }`
- [`no-unused-vars`][rule-no-unused-vars]: Set new options...
  - `args: 'after-used'`
  - `caughtErrors: 'none'`
  - `ignoreRestSiblings: false`

#### Disabled Rules

- Disabled [`array-bracket-spacing`][rule-array-bracket-spacing] *(Disabled until more specific rule options are added)*.
- Disabled [`object-curly-newline`][rule-object-curly-newline].

#### Misc

- Updated NPM peer/dev dependencies to use latest version of ESLint (v4.4.1).



## [v2.1.5](https://github.com/bsara/eslint-config-bsara/tree/v2.1.5) - 2017-07-26

#### Disabled Rules

- Disabled [`array-bracket-newline`][rule-array-bracket-newline].
- Disabled [`array-element-newline`][rule-array-element-newline].



## [v2.1.4](https://github.com/bsara/eslint-config-bsara/tree/v2.1.4) - 2017-07-21

#### New Rules

- Began adding rules from [`eslint-plugin-import`][plugin-import]

#### Fixes

- Fixed filenames and id names rule regex (forgot to escape my slashes!)
- Fixed issues with [`array-element-newline`][rule-array-element-newline] and [`no-multi-spaces`][rule-no-multi-spaces] rules

#### Misc

- Moved Node/CommonJS rules back into main config
- Updated NPM peer/dev dependencies to use latest version of ESLint (v4.3.0).



## [v2.0.0](https://github.com/bsara/eslint-config-bsara/tree/v2.0.0) - 2017-07-12

#### New Rules

- [`array-bracket-newline`][rule-array-bracket-newline] = `[ 'error', { multiline: true } ]`
- [`array-element-newline`][rule-array-element-newline] = '[ 'error', { multiline: true } ]`
- [`for-direction`][rule-for-direction] = `'warn'`
- [`getter-return`][rule-getter-return] = `'error'`
- [`no-buffer-constructor`][rule-no-buffer-constructor] = `'error'`
- [`nonblock-statement-body-position`][rule-nonblock-statement-body-position] = `[ 'error', 'beside' ]`
- [`object-curly-newline`][rule-object-curly-newline] = `[ 'error', { multiline: true } ]`
- [`padding-line-between-statements`][rule-padding-line-between-statements] = *Too much to list here, see `index.js` for details*
- [`semi-style`][rule-semi-style] = `'error'`

#### Updated Rules

- [`filenames/match-regex`][rule-filenames-match-regex]: Updated to allow `.` characters in file names
  (including at the beginning of file names).
- [`indent`][rule-indent]: Added the options found below.
    - `ArrayExpression: 'first'`
    - `ObjectExpression: 'first'`

#### Removed Rules

- [`lines-around-directive`][rule-lines-around-directive] *(This rule was deprecated as of ESLint 4.0.0)*

#### Misc

- Change package id to `@bsara/eslint-config`.
- Created new `node` configuration which can be used on top of the current default config (**NOTE:** all node/commonjs
  rule have been moved to the new node config, to continue using them, you must extend BOTH `@bsara` and `@bsara/node`).
- Removed default `env` value from default config.
- Added some npm script helpers for future releases.
- Updated NPM peer/dev dependencies to use latest version of ESLint (v4.2.0).
- Updated NPM dependency to use latest version of [eslint-plugin-filenames][plugin-filenames] (v1.2.0).
- Fixed bugs URL in `package.json`.



## [v1.1.0](https://github.com/bsara/eslint-config-bsara/tree/v1.1.0) - 2017-02-24

#### New Rules

- [`semi-spacing`][rule-semi-spacing] = `'error', { before: false }`

#### Misc

- Updated NPM peer/dev dependencies to use latest version of ESLint (v3.16.1).




## [v1.0.0](https://github.com/bsara/eslint-config-bsara/tree/v1.0.0) - 2016-11-19

- Initial Release






[plugin-import]:    https://www.npmjs.com/package/eslint-plugin-import    "eslint-plugin-import"
[plugin-filenames]: https://www.npmjs.com/package/eslint-plugin-filenames "eslint-plugin-filenames"


[rule-filenames-match-regex]: https://github.com/selaux/eslint-plugin-filenames#consistent-filenames-via-regex-match-regex "ESLint Rule: filesnames/match-regex"

[rule-array-bracket-newline]:            http://eslint.org/docs/rules/array-bracket-newline            "ESLint Rule: array-bracket-newline"
[rule-array-bracket-spacing]:            http://eslint.org/docs/rules/array-bracket-spacing            "ESLint Rule: array-bracket-spacing"
[rule-array-element-newline]:            http://eslint.org/docs/rules/array-element-newline            "ESLint Rule: array-element-newline"
[rule-brace-style]:                      http://eslint.org/docs/rules/brace-style                      "ESLint Rule: brace-style"
[rule-for-direction]:                    http://eslint.org/docs/rules/for-direction                    "ESLint Rule: for-direction"
[rule-getter-return]:                    http://eslint.org/docs/rules/getter-return                    "ESLint Rule: getter-return"
[rule-indent]:                           http://eslint.org/docs/rules/indent                           "ESLint Rule: indent"
[rule-lines-around-directive]:           http://eslint.org/docs/rules/lines-around-directive           "ESLint Rule: lines-around-directive"
[rule-no-buffer-constructor]:            http://eslint.org/docs/rules/no-buffer-constructor            "ESLint Rule: no-buffer-constructor"
[rule-no-multi-spaces]:                  http://eslint.org/docs/rules/no-multi-spaces                  "ESLint Rule: no-multi-spaces"
[rule-no-unused-vars]:                   http://eslint.org/docs/rules/no-unused-vars                   "ESLint Rule: no-unused-vars"
[rule-nonblock-statement-body-position]: http://eslint.org/docs/rules/nonblock-statement-body-position "ESLint Rule: nonblock-statement-body-position"
[rule-object-curly-newline]:             http://eslint.org/docs/rules/object-curly-newline             "ESLint Rule: object-curly-newline"
[rule-padding-line-between-statements]:  http://eslint.org/docs/rules/padding-line-between-statements  "ESLint Rule: padding-line-between-statements"
[rule-semi-spacing]:                     http://eslint.org/docs/rules/semi-spacing                     "ESLint Rule: semi-spacing"
[rule-semi-style]:                       http://eslint.org/docs/rules/semi-style                       "ESLint Rule: semi-style"
