/**
 * @bsara/eslint-config v2.3.0-beta.2
 *
 * ISC License (ISC)
 *
 * Copyright (c) 2017, Brandon D. Sara (http://bsara.pro/)
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
 * REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
 * INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
 * LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
 * OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
module.exports = {
  extends: 'eslint:recommended',


  plugins: [
    'filenames',
    'import',
    'json'
  ],


  settings: {
    'import/ignore': [
      '\\.(css|scss|less)$',
      '\\.(png|jpeg|jpg|svg)$',
      '\\.(html|hbs|jst|ejs)$'
    ]
  },



  rules: {

    // Possible Errors
    // ----------------------------------------------------------

    // Errors
    'getter-return':       'error',
    'no-unsafe-negation':  'error',
    'no-compare-neg-zero': 'error',

    // Warnings
    'for-direction':               'warn',
    'no-template-curly-in-string': 'warn',
    'valid-jsdoc':                 [ 'warn', {
                                     requireParamDescription:  false,
                                     requireReturn:            false,
                                     requireReturnDescription: false,
                                     requireReturnType:        false,
                                     prefer: {
                                       augments:  'extends',
                                       arg:       'param',
                                       argument:  'param',
                                       exception: 'throws',
                                       fires:     'emits',
                                       host:      'external',
                                       return:    'returns',
                                       virtual:   'abstract'
                                     },
                                     preferType: {
                                       array:    'Array',
                                       boolean:  'Boolean',
                                       function: 'Function',
                                       number:   'Number',
                                       object:   'Object',
                                       string:   'String'
                                     }
                                   }],



    // Best Practices
    // ----------------------------------------------------------

    // Errors
    'array-callback-return':   'error',
    'block-scoped-var':        'error',
    'curly':                   'error',
    'dot-location':            [ 'error', 'property' ],
    'eqeqeq':                  [ 'error', 'always', { 'null': 'ignore' } ],
    'no-else-return':          'error',
    'no-extra-bind':           'error',
    'no-floating-decimal':     'error',
    'no-implicit-coercion':    [ 'error', { allow: [ '~' ] } ],
    'no-invalid-this':         'error',
    'no-iterator':             'error',
    'no-labels':               'error',
    'no-lone-blocks':          'error',
    'no-multi-spaces':         [ 'error', { ignoreEOLComments: true, exceptions: { Property: true, VariableDeclarator: true, ImportDeclaration: true } } ],
    'no-multi-str':            'error',
    'no-octal-escape':         'error',
    'no-proto':                'error',
    'no-return-assign':        'error',
    'no-return-await':         'error',
    'no-script-url':           'error',
    'no-self-compare':         'error',
    'no-throw-literal':        'error',
    'no-unused-expressions':   'error',
    'no-useless-call':         'error',
    'no-useless-computed-key': 'error',
    'no-useless-constructor':  'error',
    'no-useless-concat':       'error',
    'no-useless-rename':       'error',
    'no-useless-return':       'error',
    'no-var':                  'error',
    'no-warning-comments':     [ 'error', { terms: [ 'temp', 'tmp' ] } ],
    'prefer-numeric-literals': 'error',
    'radix':                   [ 'error', 'as-needed' ],
    'wrap-iife':               [ 'error', 'inside' ],
    'yoda':                    [ 'error', 'never', { exceptRange: true } ],

    // Warnings
    'class-methods-use-this':       'warn',
    'complexity':                   'warn',
    'consistent-return':            'warn',
    'guard-for-in':                 'warn',
    'no-caller':                    'warn',
    'no-case-declarations':         'warn',
    'no-empty-function':            'warn',
    'no-eval':                      'warn',
    'no-implied-eval':              'warn',
    'no-loop-func':                 'warn',
    'no-new-func':                  'warn',
    'no-unmodified-loop-condition': 'warn',
    'no-void':                      'warn',
    'no-with':                      'warn',
    'prefer-spread':                'warn',
    'prefer-template':              'warn',

    // Disabled
    'no-fallthrough': 'off',



    // Strict Mode
    // ----------------------------------------------------------

    // Disabled
    'strict': 'off',



    // Variables
    // ----------------------------------------------------------

    // Errors
    'no-catch-shadow':            'error',
    'no-shadow-restricted-names': 'error',
    'no-unused-vars':             [ 'error', {
                                    args:               'after-used',
                                    caughtErrors:       'none',
                                    ignoreRestSiblings: false
                                  }],
    'no-use-before-define':       [ 'error', 'nofunc' ],

    // Warnings
    'no-shadow': [ 'warn', { builtinGlobals: true, allow: [ 'event', 'require' ] } ],



    // Node.js and CommonJS
    // ----------------------------------------------------------

    // Errors
    'global-require':        'error',
    'no-buffer-constructor': 'error',
    'no-new-require':        'error',

    // Warnings
    'no-path-concat':  'warn',
    'no-process-exit': 'warn',



    // Stylistic Issues
    // ----------------------------------------------------------

    // Errors
    'arrow-spacing':                    'error',
    'block-spacing':                    [ 'error', 'always' ],
    'brace-style':                      [ 'error', '1tbs', { allowSingleLine: true } ],
    'comma-dangle':                     [ 'error', 'never' ],
    'comma-spacing':                    'error',
    'comma-style':                      [ 'error', 'last' ],
    'computed-property-spacing':        [ 'error', 'never' ],
    'eol-last':                         [ 'error', 'unix' ],
    'func-call-spacing':                'error',
    'generator-star-spacing':           [ 'error', { before: false, after: true } ],
    'id-blacklist':                     [ 'error', 'that', '_that' ],
    'id-match':                         [ 'error', '^(((s_|_{0,2})([A-Za-z\\$][A-Za-z\\$0-9]*){0,1}_{0,2})|([A-Z]+(_[A-Z]+)*))$', { properties: true } ],
    'indent':                           [ 'error', 2, {
                                          ArrayExpression:        'first',
                                          CallExpression:         { arguments:  'first' },
                                          flatTernaryExpressions: false,
                                          FunctionDeclaration:    { body: 1, parameters: 'first' },
                                          FunctionExpression:     { body: 1, parameters: 'first' },
                                          ImportDeclaration:      'first',
                                          outerIIFEBody:          1,
                                          ObjectExpression:       'first',
                                          SwitchCase:             1,
                                          VariableDeclarator:     { var: 2, let: 2, const: 3 }
                                        }],
    'jsx-quotes':                       [ 'error', 'prefer-double' ],
    'key-spacing':                      [ 'error', { align: 'value' } ],
    'keyword-spacing':                  'error',
    'linebreak-style':                  [ 'error', 'unix' ],
    'max-depth':                        [ 'error', { max: 3 } ],
    'max-nested-callbacks':             [ 'error', { max: 3 } ],
    'max-statements-per-line':          [ 'error', { max: 2 } ],
    'new-cap':                          [ 'error', { capIsNew: false } ],
    'new-parens':                       'error',
    'no-confusing-arrow':               [ 'error', { allowParens: true } ],
    'no-duplicate-imports':             [ 'error', { includeExports: true } ],
    'no-lonely-if':                     'error',
    'no-multiple-empty-lines':          [ 'error', { max: 1000, maxEOF: 1 } ],
    'no-nested-ternary':                'error',
    'no-tabs':                          'error',
    'no-trailing-spaces':               'error',
    'no-unneeded-ternary':              'error',
    'no-whitespace-before-property':    'error',
    'nonblock-statement-body-position': [ 'error', 'beside' ],
    'object-curly-spacing':             [ 'error', 'always' ],
    'object-shorthand':                 [ 'error', 'always', { avoidQuotes: true, ignoreConstructors: true } ],
    'one-var-declaration-per-line':     [ 'error', 'always' ],
    'one-var':                          [ 'error', 'never' ],
    'operator-linebreak':               [ 'error', 'before', {
                                          overrides: {
                                            '=':    'never',
                                            '+=':   'never',
                                            '-=':   'never',
                                            '*=':   'never',
                                            '/=':   'never',
                                            '%=':   'never',
                                            '<<=':  'never',
                                            '>>=':  'never',
                                            '>>>=': 'never',
                                            '&=':   'never',
                                            '^=':   'never',
                                            '|=':   'never',
                                            '==':   'never',
                                            '===':  'never',
                                            '!=':   'never',
                                            '!==':  'never',
                                            '>=':   'never',
                                            '<=':   'never',
                                            '>':    'never',
                                            '<':    'never'
                                          }
                                        }],
    'padding-line-between-statements':  [ 'error',
                                          {
                                            blankLine: 'always',
                                            prev:      '*',
                                            next: [
                                              'cjs-export',
                                              'class',
                                              'export',
                                              'function'
                                            ]
                                          },
                                          {
                                            blankLine: 'always',
                                            prev: [
                                              'cjs-export',
                                              'cjs-import',
                                              'class',
                                              'directive',
                                              'export',
                                              'function',
                                              'import'
                                            ],
                                            next: '*'
                                          },
                                          { blankLine: 'never', prev: '*',          next: 'directive' },
                                          { blankLine: 'any',   prev: 'cjs-export', next: 'cjs-export' },
                                          { blankLine: 'any',   prev: 'cjs-import', next: 'cjs-import' },
                                          { blankLine: 'any',   prev: 'directive',  next: 'directive' },
                                          { blankLine: 'any',   prev: 'export',     next: 'export' },
                                          { blankLine: 'any',   prev: 'import',     next: 'import' },
                                        ],
    'rest-spread-spacing':              [ 'error', 'never' ],
    'semi-spacing':                     [ 'error', { before: false } ],
    'semi':                             [ 'error', 'always' ],
    'semi-style':                       'error',
    'space-before-blocks':              'error',
    'space-before-function-paren':      [ 'error', { anonymous: 'never', named: 'never', asyncArrow: 'always' } ],
    'space-in-parens':                  'error',
    'space-infix-ops':                  'error',
    'space-unary-ops':                  'error',
    'spaced-comment':                   [ 'error', 'always', {
                                          line: { exceptions: [ '-' ] },
                                          block: {
                                            exceptions: [ '*' ],
                                            balanced:   true
                                          }
                                        }],
    'template-curly-spacing':           'error',
    'wrap-regex':                       'error',
    'yield-star-spacing':               [ 'error', 'after' ],

    // Warnings
    'max-len':   [ 'warn', { code: 10000000, tabWidth: 2, comments: 90, ignoreTrailingComments: true, ignoreRegExpLiterals: true } ],
    'max-lines': [ 'warn', { max: 1000, skipBlankLines: true } ],

    // Disabled
    'array-bracket-spacing': 'off', // [ 'error', 'always', { singleValue: true, objectsInArrays: true, arraysInArrays: true } ], // TODO: re-enable once more specific rules are added
    'array-bracket-newline': 'off',
    'array-element-newline': 'off',
    'object-curly-newline':  'off',



    // Plugin: eslint-plugin-filenames
    // ----------------------------------------------------------

    // Errors
    'filenames/match-regex': [ 'error', '^(_{1,2}|\\.|[a-z\\$])([a-z0-9\\$]|[a-z0-9\\$]{1}\\-[a-z0-9\\$]{1}|[a-z0-9\\$]{1}\\.[a-z0-9\\$]{1})*?$' ],



    // Plugin: eslint-plugin-import (Static Analysis)
    // ----------------------------------------------------------

    // Errors
    'import/named':                    'error',
    'import/namespace':                [ 'error', { allowComputed: true } ],
    'import/no-unresolved':            [ 'error', { caseSensitive: true, commonjs: true, amd: true } ],
    'import/no-webpack-loader-syntax': 'error',

    // Warnings
    'import/no-absolute-path': [ 'warn', { esmodule: true, commonjs: true, amd: true }], // TODO: ???

    // Disabled
    'import/default':             'off',
    'import/no-dynamic-require':  'off',
    'import/no-internal-modules': 'off',
    'import/no-restricted-paths': 'off',


    // Plugin: eslint-plugin-import (Helpful Warnings)
    // ----------------------------------------------------------

    // Errors
    'import/export':                     'error',
    'import/no-extraneous-dependencies': [ 'error', { devDependencies: true, optionalDependencies: true, peerDependencies: false } ],

    // Warnings
    'import/deprecated':          'warn',
    'import/no-mutable-exports':  'warn',
    'import/no-named-as-default': 'warn',

    // Disabled
    'import/no-named-as-default-member': 'off',


    // Plugin: eslint-plugin-import (Module Systems)
    // ----------------------------------------------------------

    // Warnings
    'import/unambiguous': 'warn',

    // Disabled
    'import/no-amd':            'off',
    'import/no-commonjs':       'off',
    'import/no-nodejs-modules': 'off',


    // Plugin: eslint-plugin-import (Style Guide)
    // ----------------------------------------------------------

    // Error
    'import/extensions':           [ 'error', 'always', { js: 'never' } ],
    'import/first':                'error',
    'import/newline-after-import': [ 'error', { count: 2 } ],
    'import/no-duplicates':        'error',
    'import/no-named-default':     'error',
    'import/order':                [ 'error', {
                                     groups: [
                                       'builtin',
                                       'external',
                                       [ 'internal', 'parent', 'sibling' ],
                                       'index'
                                     ],
                                     'newlines-between': 'always-and-inside-groups'
                                   }],

    // Warnings
    'import/max-dependencies':            [ 'warn', { max: 10 } ],
    'import/no-anonymous-default-export': 'warn',
    'import/prefer-default-export':       'warn',

    // Disabled
    'import/no-namespace':         'off',
    'import/no-unassigned-import': 'off'
  }
};
